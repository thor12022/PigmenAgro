package thor12022.pigmenagro.command;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandHandler;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.translation.I18n;
import thor12022.pigmenagro.ModInformation;

public class CommandManager extends CommandBase
{
   private Map<String, AbstractSubCommand> commandMap = new HashMap<>();
   
   public void register(CommandHandler commandHandler)
   {
      commandHandler.registerCommand(this);
   }
   
   @Override
   public String getCommandName()
   {
      return ModInformation.CHANNEL;
   }

   @Override
   public String getCommandUsage(ICommandSender sender)
   {
      String text = I18n.translateToLocal("command." + getCommandName() + ".usage") + "\n";
      for(String subCommand : commandMap.keySet())
      {
         text += subCommand + "\n";
      }
      return text;
   }

   /**
    * Return the required permission level for this command.
    */
   @Override
   public int getRequiredPermissionLevel()
   {
       return 2;
   }
   
   @Override
   public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws WrongUsageException
   {
      if (args.length == 0)
      {
          throw new WrongUsageException(getCommandUsage(sender));
      }
      else if (args[0].equals("help"))
      {
          throw new WrongUsageException(getCommandUsage(sender));
      }
      else if (commandMap.containsKey(args[0]))
      {
         
         commandMap.get(args[0]).execute(sender, ArrayUtils.subarray(args, 1, args.length - 1));
      }
   }

   public void registerSubCommand(AbstractSubCommand subCommand)
   {
      commandMap.put(subCommand.getCommandName(), subCommand);
   }
   
}
