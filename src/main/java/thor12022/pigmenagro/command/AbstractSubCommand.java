package thor12022.pigmenagro.command;

import java.util.Arrays;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import thor12022.pigmenagro.PigmenAgro;

public abstract class AbstractSubCommand extends CommandBase implements ISubCommand
{
   @Override
   public String getCommandUsage(ICommandSender sender)
   {
      return "commands." + getCommandName() + ".usage";
   }
   
   @Override
   public void execute(MinecraftServer server, ICommandSender sender, String[] args) 
   {
      PigmenAgro.LOGGER.debug(Arrays.toString(Thread.currentThread().getStackTrace()));
   }
}
