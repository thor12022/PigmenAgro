package thor12022.pigmenagro.command;

import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

public interface ISubCommand
{
   public void execute(ICommandSender sender, String[] args) throws WrongUsageException;
}
