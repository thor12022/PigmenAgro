package thor12022.pigmenagro.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapSet<Key, Value>
{
   private final Map<Key, Set<Value>> mapSet= new HashMap<>();
   
   /**
    * @param key
    * @param value
    * @return true if the key:value pair did not exist
    */
   public boolean put(Key key, Value value)
   {
      if(!mapSet.containsKey(key))
      {
         mapSet.put(key, new HashSet<>());
      }
      return mapSet.get(key).add(value);
   }
   
   public boolean containsKey(Key key)
   {
      return mapSet.containsKey(key);
   }
   
   public boolean containsValue(Key key, Value value)
   {
      return mapSet.containsKey(key) ? mapSet.get(key).contains(value) : false;
   }
   
   public void clear()
   {
      mapSet.clear();
   }

   public boolean remove(Key key)
   {
      return mapSet.remove(key) != null;
   }
}
