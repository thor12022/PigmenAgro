package thor12022.pigmenagro;

import java.util.List;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;
import thor12022.pigmenagro.config.Config;
import thor12022.pigmenagro.config.Configurable;
import thor12022.pigmenagro.entity.EntityExplosionTrigger;
import thor12022.pigmenagro.entity.PigmenHelper;

@Configurable
public class EventHandler
{
   @Config(minInt = 1, maxInt = 256, comment = "Changing too high will have an effect on performance")
   private int agroRange = 48;

   public EventHandler()
   {
      PigmenAgro.CONIG.register(this);
      if(PigmenHelper.isValid())
      {
         MinecraftForge.EVENT_BUS.register(this);
      }
      else
      {
         PigmenAgro.LOGGER.error("Cannot anger Zombie Pigmen, disabling mod functionality");
      }
   }
      
   @SubscribeEvent
   public void blockBroken(HarvestDropsEvent event)
   {
      if(!event.getWorld().isRemote && !event.isSilkTouching() && event.getHarvester() != null && !event.getHarvester().capabilities.isCreativeMode)
      {
         try
         {
            if(PigmenAgro.API.isPigmenAgroBlock(event.getState()))
            {
               PigmenAgro.LOGGER.info(event.getHarvester().getDisplayName() + " Angered the Zombie Pigmen");
               List<EntityPigZombie> list = event.getWorld().getEntitiesWithinAABB(EntityPigZombie.class, new AxisAlignedBB(event.getPos()).expandXyz(agroRange));
               for(EntityPigZombie entitypigzombie : list)
               {
                  if(!entitypigzombie.isAngry())
                  {
                     if(!PigmenHelper.anger(entitypigzombie, event.getHarvester()))                  
                     {
                        PigmenAgro.LOGGER.error("Cannot anger Zombie Pigmen, disabling mod functionality");
                        MinecraftForge.EVENT_BUS.unregister(this);
                        break;
                     }
                  }
               }
               EntityExplosionTrigger.spawn(event.getWorld(), event.getPos(), event.getFortuneLevel());          
            }
         }
         catch(InvalidApiUsageException e)
         {
            PigmenAgro.LOGGER.error(e);
            PigmenAgro.LOGGER.error("Error with PigmenAgro API, disabling mod functionality");
            MinecraftForge.EVENT_BUS.unregister(this);
         }
      }
   }
}
