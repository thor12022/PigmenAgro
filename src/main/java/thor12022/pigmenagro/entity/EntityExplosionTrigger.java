package thor12022.pigmenagro.entity;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import thor12022.pigmenagro.PigmenAgro;
import thor12022.pigmenagro.config.Config;
import thor12022.pigmenagro.config.Configurable;

public class EntityExplosionTrigger extends Entity
{
   private static final String UNLOCALIZED_NAME = "explosionTrigger";
 
   @Configurable(sectionName="ExplodingOre")
   static class ConfigData
   {
      @Config(minInt = 0)
      int ticksUntilExplode = 40;
      
      @Config(minFloat = 0f, comment="Too high a value can cause server crashes")
      float explosionStrengthMultiplier = 0.5f;
      
      @Config(minFloat = 0f, comment="Too high a value can cause server crashes")
      float explosionStrengthBase = 0.6f;
      
      @Config(minFloat = 0f, maxFloat = 1f)
      float explodeChance = 0.25f;
      
      @Config
      boolean explosionsBurn = true;
      
      @Config 
      boolean explosionsBreak = true;
   }
   
   private static final ConfigData CONFIG = new ConfigData();
   
   static
   {
      PigmenAgro.CONIG.register(CONFIG);
   }
   
   static void register(int id)
   {
      EntityRegistry.registerModEntity(
            EntityExplosionTrigger.class,
            UNLOCALIZED_NAME,
            id, 
            PigmenAgro.INSTANCE, 
            8, 
            10, 
            false);
   }
   
   public static void spawn(World world, BlockPos pos, int strengthLevel)
   {
      if(!world.isRemote && PigmenAgro.RANDOM.nextFloat() < CONFIG.explodeChance)
      {
         EntityExplosionTrigger explosionTrigger = new EntityExplosionTrigger(world, strengthLevel);
         explosionTrigger.setLocationAndAngles(pos.getX() + .5f, pos.getY() + .5f, pos.getZ() + .5f, 0.0F, 0.0F);
         world.spawnEntityInWorld(explosionTrigger);
      }
   }
   
   private float explosionStrength;
      
   EntityExplosionTrigger(World worldIn, int strengthLevel)
   {
      super(worldIn);
      explosionStrength = CONFIG.explosionStrengthBase + strengthLevel * CONFIG.explosionStrengthMultiplier;
   }

   @Override
   public void onUpdate()
   {    
      if(worldObj.isRemote && firstUpdate)
      {

         worldObj.playSound(posX, posY, posZ, SoundEvent.soundEventRegistry.getObject(new ResourceLocation("fire.ignite")), SoundCategory.BLOCKS, 1.5f, 1.0f, addedToChunk);
      }
      else if(ticksExisted >= CONFIG.ticksUntilExplode)
      {
         if(!worldObj.isRemote)
         {            
            worldObj.newExplosion(this, posX, posY, posZ, explosionStrength, CONFIG.explosionsBurn, CONFIG.explosionsBreak);
            worldObj.removeEntity(this);
         }
      }
      else if(worldObj.isRemote)
      {
         int smokeParticles = (int) Math.ceil(5 * ((double)ticksExisted / CONFIG.ticksUntilExplode));
         for(int i = 0; i < smokeParticles; ++i)
         {
            double x = posX + rand.nextDouble()  - .5D;
            double y = posY + rand.nextDouble() * 0.5D + 0.5D;
            double z = posZ + rand.nextDouble()  - .5D;
            worldObj.spawnParticle(EnumParticleTypes.SMOKE_LARGE, x, y, z, 0.0D, 0.0D, 0.0D, new int[0]);
         }
      }
      super.onUpdate();
   }

   @Override
   protected void entityInit()
   {
      setSize(0f, 0f);
   }

   @Override
   public void readEntityFromNBT(NBTTagCompound tagCompound)
   {
      explosionStrength = tagCompound.getFloat("explosionStrength");
   }

   @Override
   public void writeEntityToNBT(NBTTagCompound tagCompound)
   {
      tagCompound.setFloat("explosionStrength", explosionStrength);
   }
}
