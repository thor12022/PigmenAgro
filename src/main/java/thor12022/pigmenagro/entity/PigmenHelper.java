package thor12022.pigmenagro.entity;

import java.lang.invoke.MethodHandle;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityPigZombie;
import thor12022.pigmenagro.PigmenAgro;
import thor12022.pigmenagro.utils.ReflectionUtils;

public class PigmenHelper
{
   private static MethodHandle becomeAngryMethod;
   
   static 
   {
      try
      {
         becomeAngryMethod = ReflectionUtils.findMethod(EntityPigZombie.class, new String[] {"func_70835_c", "becomeAngryAt"}, Entity.class);
      }
      catch(Exception exp)
      {
         PigmenAgro.LOGGER.error(exp);
         PigmenAgro.LOGGER.error("Cannot find method to anger Zombie Pigmen, disabling agroing functionality");
      }
   }
   
   public static boolean isValid()
   {
      return becomeAngryMethod != null;
   }
   
   public static boolean anger(EntityPigZombie zombiePigman, EntityLivingBase target)
   {
      try
      {
         becomeAngryMethod.invoke(zombiePigman, target);
         return true;
      }
      catch(@SuppressWarnings("unused") Throwable e)
      {
         return false;
      }
   }
}
