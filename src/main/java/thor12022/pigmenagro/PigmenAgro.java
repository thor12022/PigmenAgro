package thor12022.pigmenagro;

import net.minecraft.command.CommandHandler;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import thor12022.pigmenagro.api.PigmenAgroApiInternal;
import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;
import thor12022.pigmenagro.command.CommandManager;
import thor12022.pigmenagro.config.ConfigManager;
import thor12022.pigmenagro.core.PigmenAgroApi;
import thor12022.pigmenagro.entity.EntityRegistry;
import thor12022.pigmenagro.registrars.MetallurgyRegistrar;
import thor12022.pigmenagro.registrars.PigmanAgroRegistrar;
import thor12022.pigmenagro.registrars.TinkersConstructRegistrar;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION, dependencies = ModInformation.DEPEND)
public class PigmenAgro
{
   @Mod.Instance
   public static PigmenAgro INSTANCE;
   
   final public static Random          RANDOM   =  new Random();
   final public static Logger          LOGGER   =  LogManager.getLogger(ModInformation.NAME);
   final public static ConfigManager   CONIG    =  new ConfigManager(ModInformation.CHANNEL);
   final public static CommandManager  COMMAND  =  new CommandManager();
   public static final PigmenAgroApi   API      =  new PigmenAgroApi();
   
   @SuppressWarnings("unused")
   private final EventHandler          eventHandler   =  new EventHandler();
   private final EntityRegistry        entityRegistry =  new EntityRegistry();
   
   private final PigmanAgroRegistrar         pigmenAgroRegistrar  =  new PigmanAgroRegistrar();
   private final MetallurgyRegistrar         metallurgyRegistrar  =  new MetallurgyRegistrar();
   private final TinkersConstructRegistrar   ticoRegistrar        =  new TinkersConstructRegistrar();

   public PigmenAgro()
   {
      try
      {
         PigmenAgroApiInternal.setGemSetHandler(API);
      }
      catch(InvalidApiUsageException e)
      {
         LOGGER.error(e);
         LOGGER.fatal("Alight, here's the deal, I'm just minding my own business trying to Construct, but apprently I can't?!");
      }
   }
   
   @Mod.EventHandler
   public void perinit(@SuppressWarnings("unused") FMLPreInitializationEvent event)
   {
      LOGGER.info(I18n.translateToLocal("info." + ModInformation.ID + ".console.load.preinit"));

      entityRegistry.registerEntities();
   }
   
   @Mod.EventHandler
   public void postinit(@SuppressWarnings("unused") FMLInitializationEvent event)
   {
      LOGGER.info(I18n.translateToLocal("info." + ModInformation.ID + ".console.load.init"));
      
      pigmenAgroRegistrar.init();
      metallurgyRegistrar.init();
      ticoRegistrar.init();
   }
   
   @Mod.EventHandler
   public void serverStarting(FMLServerStartingEvent event)
   {
      LOGGER.info(I18n.translateToLocal("info." + ModInformation.ID + ".console.load.serverstarting"));

      COMMAND.register((CommandHandler)event.getServer().getCommandManager());
   }
}
