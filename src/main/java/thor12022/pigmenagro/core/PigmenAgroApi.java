package thor12022.pigmenagro.core;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.oredict.OreDictionary;
import thor12022.pigmenagro.api.IPigmenAgroApi;
import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;
import thor12022.pigmenagro.utils.MapSet;

public class PigmenAgroApi implements IPigmenAgroApi
{
   
   private static class BlockMapSet
   {
      private final Map<Block, Boolean>      isWild   =  new HashMap<>();
      private final MapSet<Block, Integer>   blocks   =  new MapSet<>();
      
      public boolean addMatch(Block block, int meta)
      {
         isWild.put(block, meta == OreDictionary.WILDCARD_VALUE);
         return blocks.put(block, meta);
      }

      public boolean matches(Block block, int meta)
      {
         if(isWild.containsKey(block))
         {
            return isWild.get(block) ? true : blocks.containsValue(block, meta);
         }
         return false;
      }

      public boolean removeMatch(Block block)
      {
         isWild.remove(block);
         return blocks.remove(block);
      }
   }
      
   private final BlockMapSet blockStates = new BlockMapSet();

   @Override
   public boolean addPigmenAgroBlock(IBlockState blockState) throws InvalidApiUsageException
   {
      if(!Loader.instance().hasReachedState(LoaderState.INITIALIZATION))
      {
         throw new InvalidApiUsageException("Cannot add Blocks prior to mod's Initialization");
      }
      
      if(blockStates.matches(blockState.getBlock(), blockState.getBlock().getMetaFromState(blockState)))
      {
         return false;
      }
      
      blockStates.addMatch(blockState.getBlock(), blockState.getBlock().getMetaFromState(blockState));
      
      return true;
   }   

   @Override
   public boolean addPigmenAgroBlock(Block block) throws InvalidApiUsageException
   {
      if(!Loader.instance().hasReachedState(LoaderState.INITIALIZATION))
      {
         throw new InvalidApiUsageException("Cannot add Blocks prior to mod's Initialization");
      }

      return blockStates.addMatch(block, OreDictionary.WILDCARD_VALUE);
   }

   @Override
   public boolean removePigmenAgroBlock(Block block) throws InvalidApiUsageException
   {
      if(!Loader.instance().hasReachedState(LoaderState.INITIALIZATION))
      {
         throw new InvalidApiUsageException("Cannot remove Blocks prior to mod's Initialization");
      }

      return blockStates.removeMatch(block);
   }
      
   @Override
   public boolean isPigmenAgroBlock(IBlockState blockState) throws InvalidApiUsageException
   {
      if(!Loader.instance().hasReachedState(LoaderState.INITIALIZATION))
      {
         throw new InvalidApiUsageException("Cannot get Blocks prior to mod's Initialization");
      }      
      return blockStates.matches(blockState.getBlock(), blockState.getBlock().getMetaFromState(blockState));
   }

      
}
