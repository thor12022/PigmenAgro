package thor12022.pigmenagro;

public class ModInformation
{

   public static final String NAME = "Pigmen Agro";
   public static final String ID = "pigmenagro";
   public static final String CHANNEL = "PigmenAgro";
   public static final String DEPEND = "required-after:Forge@*";
   public static final String VERSION = "%VERSION%";
}
