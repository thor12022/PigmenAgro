package thor12022.pigmenagro.api;

import net.minecraftforge.fml.common.Loader;
import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;

/**
 * The should be removed from the API Jar, and should not be used
 *
 */
public class PigmenAgroApiInternal
{
   public static void setGemSetHandler(IPigmenAgroApi gemSetApi) throws InvalidApiUsageException
   {
      if(Loader.instance().activeModContainer().getMod() == PigmenAgro.instance)
      {
         PigmenAgro.instance = gemSetApi;
      }
      else
      {
         throw new InvalidApiUsageException("Improper API Initialization");
      }
   }
}
