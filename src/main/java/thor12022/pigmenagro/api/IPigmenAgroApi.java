package thor12022.pigmenagro.api;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;

/**
 * This should not be implemented
 */
public interface IPigmenAgroApi
{
   /**
    * Used to match a Blockstate without regard to properties
    * @param blockState
    * @return false if blockState would already be matched
    * @throws InvalidApiUsageException if FML not in or after Initialization state
    */
   boolean addPigmenAgroBlock(IBlockState blockState) throws InvalidApiUsageException;
   
   /**
    * Used to match a Block without regard to state or properties
    * @param block 
    * @return false if block would already be matched without regard to state or properties
    * @throws InvalidApiUsageException if FML not in or after Initialization state
    */
   boolean addPigmenAgroBlock(Block block) throws InvalidApiUsageException;
   
   /**
    * Used to match a Block without regard to state or properties
    * @param block 
    * @return false if block was not removed
    * @throws InvalidApiUsageException if FML not in or after Initialization state
    */
   boolean removePigmenAgroBlock(Block block) throws InvalidApiUsageException;

   /**
    * Used to match a Blockstate without regard to properties
    * @param blockState
    * @return false if blockState does not exist
    * @throws InvalidApiUsageException if FML not in or after Initialization state
    */
   boolean isPigmenAgroBlock(IBlockState blockState) throws InvalidApiUsageException;
}
