package thor12022.pigmenagro.api;

import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;

public class PigmenAgro
{ 
   static IPigmenAgroApi instance;
   
   public static IPigmenAgroApi getApi() throws InvalidApiUsageException
   {
      if(instance != null)
      {
         return instance;
      }
      throw new InvalidApiUsageException("The Gem Set API has not been initialized yet");
   }
}
