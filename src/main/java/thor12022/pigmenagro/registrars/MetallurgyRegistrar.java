package thor12022.pigmenagro.registrars;

import net.minecraftforge.fml.common.Loader;
import thor12022.pigmenagro.PigmenAgro;
import thor12022.pigmenagro.config.Config;
import thor12022.pigmenagro.config.Configurable;


@Configurable(sectionName="Metallurgy")
public class MetallurgyRegistrar
{
   private final static String METALLURGY_ID = "metallurgy";
   
   @Config(comment = "Metallurgy 5 support not implemented, this will not work")
   private boolean enable = false;
   
   public MetallurgyRegistrar()
   {
      PigmenAgro.CONIG.register(this);
   }
      
   public void init()
   {
      if(enable && Loader.isModLoaded(METALLURGY_ID))
      {
         /*
         IMetalSet metalSet = MetallurgyApi.getMetalSet("nether");
         for(String metal : metalSet.getMetalNames())
         {
            ItemStack metalOreStack = metalSet.getOre(metal);
            if(metalOreStack != null)
            {
               Block metalOreBlock = Block.getBlockFromItem(metalOreStack.getItem());
               idToMetadataMap.put(metalOreBlock.getLocalizedName(), metalOreStack.getItemDamage());
               PigmenAgro.logger
                     .debug("Adding " + metalOreStack.getItem().getItemStackDisplayName(metalOreStack) + " for "
                           + getClass().getSimpleName());
            }
            else
            {
               PigmenAgro.logger.warn(
                     "Error getting " + metal + " for Metallurgy Compatibility for " + getClass().getSimpleName());
            }
         }
         */
      } 
   }
}
