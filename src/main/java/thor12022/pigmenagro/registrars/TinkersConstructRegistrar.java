package thor12022.pigmenagro.registrars;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.oredict.OreDictionary;
import thor12022.pigmenagro.PigmenAgro;
import thor12022.pigmenagro.api.exceptions.InvalidApiUsageException;
import thor12022.pigmenagro.config.Config;
import thor12022.pigmenagro.config.Configurable;

@Configurable(sectionName="TinkersConstruct")
public class TinkersConstructRegistrar
{
   private final static String TINKERS_ID = "tconstruct";
   
   private final static String[] ORE_NAMES = 
      {
        "oreCobalt",
        "oreArdite"
      };
   
   @Config(comment="Experimental")
   private boolean enable = false;
   
   public TinkersConstructRegistrar()
   {
      PigmenAgro.CONIG.register(this);
   }
      
   public void init()
   {
      if(enable && Loader.isModLoaded(TINKERS_ID))
      {
         List<ItemStack> ores = new ArrayList<>();
         
         for(String ore : ORE_NAMES)
         {
            ores.addAll(OreDictionary.getOres(ore));
         }
         
         for(ItemStack itemStack : ores)
         {
            if(itemStack != null)
            {
               Item item = itemStack.getItem();
               if(item != null)
               {
                  Block block = Block.getBlockFromItem(item);
                  try
                  {
                     PigmenAgro.API.addPigmenAgroBlock(block.getStateFromMeta(itemStack.getMetadata()));
                  }
                  catch(InvalidApiUsageException e)
                  {
                     PigmenAgro.LOGGER.error(e);
                     PigmenAgro.LOGGER.warn("Cannot add " + block.getRegistryName() + " with meta " + itemStack.getMetadata());
                  }
               }
            }               
         }
      } 
   }
}
