package thor12022.pigmenagro.registrars;

import com.google.gson.*;

import java.io.File;

import org.apache.commons.io.FileUtils;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraftforge.oredict.OreDictionary;
import thor12022.pigmenagro.PigmenAgro;
import thor12022.pigmenagro.command.AbstractSubCommand;

public class PigmanAgroRegistrar
{
   private static final String JSON_FILE_NAME = "Blocks.json";
   private static final String DEFAULT_JSON = "[\n\t{\n\t\t\"BlockName\" : \"glowstone\"\n   },\n\t{\n\t\t\"BlockName\" : \"quartz_ore\"\n\t}\n]";
   
   AbstractSubCommand reloadCommand = new AbstractSubCommand()
   {
      private final String NAME = "reloadBlocks";
      
      @Override
      public void execute(ICommandSender sender, String[] args) throws WrongUsageException
      {
         readJson();
      }

      @Override
      public String getCommandName()
      {
         return NAME;
      }
   };
   
   public void init()
   {
      PigmenAgro.COMMAND.registerSubCommand(reloadCommand);
      readJson();
   }
   
   public void readJson()
   {
      File jsonFile = new File(PigmenAgro.CONIG.getConfigFile().getParent() + 
                               File.separator + JSON_FILE_NAME);
      try
      {
         if(!jsonFile.exists())
         {
            FileUtils.writeStringToFile(jsonFile, DEFAULT_JSON);
         }
         JsonElement rootElement = new JsonParser().parse(FileUtils.readFileToString(jsonFile));
         JsonArray rootArray = rootElement.getAsJsonArray();
         for(JsonElement jElement : rootArray)
         {
            JsonObject jObject = jElement.getAsJsonObject();
            String blockName = jObject.get("BlockName").getAsString();
            int metaData = OreDictionary.WILDCARD_VALUE;
            try
            {
               metaData = jElement.getAsJsonObject().getAsJsonObject("MetaData").getAsInt();
            }
            catch(@SuppressWarnings("unused") Exception e)
            {
               // It's ok to not have metadata
            }
            Block block = Block.getBlockFromName(blockName);
            if(block != null)
            {
               IBlockState blockState = block.getStateFromMeta(metaData);
               if(blockState != null)
               {
                  PigmenAgro.API.addPigmenAgroBlock(blockState);
               }
               else
               {
                  PigmenAgro.LOGGER.warn("PigmanAgroHandler cannot find: " + blockName + ":" + metaData);
               }
            }
            else
            {
               PigmenAgro.LOGGER.warn("PigmanAgroHandler cannot find: " + blockName);
            }
         }
         PigmenAgro.LOGGER.info("PigmanAgroHandler reloaded " + jsonFile.getAbsolutePath());
      }
      catch(Exception exp)
      {
         PigmenAgro.LOGGER.warn("PigmanAgroHandler cannot read " + jsonFile.toString() + " : " + exp);
      }
   }
}
